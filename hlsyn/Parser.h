#ifndef Parser_H
#define	Parser_H
#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <string>
#include "Constants.h"
#include "Operation.h"
#include "Variable.h"
#include "Port.h"
using namespace std;

class Parser{
private:
	static void setGrammer(vector<string> &declarations, vector<string> &operations, vector<string> &DataWidthAndType);
	static bool isDeclaration(vector<string> &declarations, vector<string> &tokens);
	static bool isOperation(vector<string> &operationTypes, vector<string> &tokens);
	static bool isDataWidthAndType(vector<string> &DataWidthAndType, vector<string> &tokens);
	static void caseSensitive(vector<string> &AllCases, vector<string> &tokens);
	static int setDataWidth(vector<string> &tokens);
	static string setDataType(vector<string> &tokens);
	static void setVariableDependency(Operation *operation, vector<Variable *> &variableList);
	static void generatePorts(string dataType, int width, string portType, vector<string> &tokens, vector<Port *> &portList);
	static void generateVariables(string dataType, int width, vector<string> &tokens, vector<Variable *> &variableList);
	//static void 
public:
	static void splitString(string &lineBuffer, vector<string> &tokens);
	static void parsingCFile(string inputCircuitFileName, vector<Port *> &portList, vector<Variable *> &variableList, vector<Operation *> &operationList);
};
#endif