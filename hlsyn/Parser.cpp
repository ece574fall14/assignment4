#include "Parser.h"

//to_string
void Parser::setGrammer(vector<string> &declarations, vector<string> &operationTypes, vector<string> &DataWidthAndType){
	declarations.push_back("input");
	declarations.push_back("output");
	declarations.push_back("variable");
	
	operationTypes.push_back("+");
	operationTypes.push_back("-");
	operationTypes.push_back("*");
	operationTypes.push_back("/");
	operationTypes.push_back("%");
	operationTypes.push_back(">");
	operationTypes.push_back("<");
	operationTypes.push_back("==");
	operationTypes.push_back("?");
	operationTypes.push_back("<<");
	operationTypes.push_back(">>");

	DataWidthAndType.push_back("Int1");
	DataWidthAndType.push_back("Int8");
	DataWidthAndType.push_back("Int16");
	DataWidthAndType.push_back("Int32");
	DataWidthAndType.push_back("Int64");
	DataWidthAndType.push_back("UInt1");
	DataWidthAndType.push_back("UInt8");
	DataWidthAndType.push_back("UInt16");
	DataWidthAndType.push_back("UInt32");
	DataWidthAndType.push_back("UInt64");
	DataWidthAndType.push_back("bool");
	DataWidthAndType.push_back("char");
	DataWidthAndType.push_back("long");
	DataWidthAndType.push_back("short");
	DataWidthAndType.push_back("int");
	DataWidthAndType.push_back("unsigned bool");
	DataWidthAndType.push_back("unsigned char");
	DataWidthAndType.push_back("unsigned long");
	DataWidthAndType.push_back("unsigned short");
	DataWidthAndType.push_back("unsigned int");
}

bool Parser::isDeclaration(vector<string> &declarations, vector<string> &tokens){
	for (int i = 0; i < tokens.size(); i++){
		for (int j = 0; j < declarations.size(); j++){
			if (tokens.at(i) == declarations.at(j))
				return true;
		}
	}
	return false;
	cout << "The declaration format is illegal!\n" << endl;
}

bool Parser::isOperation(vector<string> &operationTypes, vector<string> &tokens){
	for (int i = 0; i < tokens.size(); i++){
		for (int j = 0; j < operationTypes.size(); j++){
			if (tokens.at(i) == operationTypes.at(j))
				return true;
		}
	}
	return false;
	cout << "The operation format is illegal!\n" << endl;
}

bool Parser::isDataWidthAndType(vector<string> &DataWidthAndType, vector<string> &tokens){
	for (int i = 0; i < tokens.size(); i++){
		for (int j = 0; j < DataWidthAndType.size(); j++){
			if (tokens.at(i) == DataWidthAndType.at(j))
				return true;
		}
	}
	return false;
	cout << "The datawidth or datatype is illegal!\n" << endl;
}

void Parser::caseSensitive(vector<string> &AllCases, vector<string> &tokens){


}

int Parser::setDataWidth(vector<string> &tokens){
	for (int i = 0; i < tokens.size(); i++){
		if (tokens.at(i) == "Int1" || tokens.at(i) == "UInt1")
			return 1;
		if (tokens.at(i) == "Int8" || tokens.at(i) == "UInt8")
			return 8;
		if (tokens.at(i) == "Int16" || tokens.at(i) == "UInt16")
			return 16;
		if (tokens.at(i) == "Int32" || tokens.at(i) == "UInt32")
			return 32;
		if (tokens.at(i) == "Int64" || tokens.at(i) == "UInt64")
			return 64;
		if (tokens.at(i) == "bool" || tokens.at(i) == "unsigned bool")
			return 1;
		if (tokens.at(i) == "char" || tokens.at(i) == "unsigned char")
			return 8;
		if (tokens.at(i) == "short" || tokens.at(i) == "unsigned short")
			return 16;
		if (tokens.at(i) == "int" || tokens.at(i) == "unsgined int")
			return 32;
		if (tokens.at(i) == "long" || tokens.at(i) == "unsigned long")
			return 64;
		return 0;
	}
}

string Parser::setDataType(vector<string> &tokens){
	for (int i = 0; i < tokens.size(); i++){
		if (tokens.at(i) == "Int1"
			|| tokens.at(i) == "Int8"
			|| tokens.at(i) == "Int16"
			|| tokens.at(i) == "Int32"
			|| tokens.at(i) == "Int64")
			return "Int";
		if (tokens.at(i) == "char"
			|| tokens.at(i) == "bool"
			|| tokens.at(i) == "short"
			|| tokens.at(i) == "long"
			|| tokens.at(i) == "int")
			return "Int";	

		if (tokens.at(i) == "UInt1"
			|| tokens.at(i) == "UInt8"
			|| tokens.at(i) == "UInt16"
			|| tokens.at(i) == "UInt32"
			|| tokens.at(i) == "UInt64")
			return "UInt";
		if (tokens.at(i) == "unsigned char"
			|| tokens.at(i) == "unsigned bool"
			|| tokens.at(i) == "unsigned short"
			|| tokens.at(i) == "unsigned long"
			|| tokens.at(i) == "unsigned int")
			return "UInt";	
	}
	return "Int";
}

void Parser::parsingCFile(string inputCircuitFileName, vector<Port *> &portList, vector<Variable *> &variableList, vector<Operation *> &operationList){
	fstream inputStream(inputCircuitFileName, ios::in);
	if (!inputStream){
		cout << "Open File " << inputCircuitFileName << " , the file does not exist." << endl;
		exit(1);
	}

	vector<string> declarations;
	vector<string> operationTypes;
	vector<string> DataWidthAndType;
	setGrammer(declarations, operationTypes, DataWidthAndType);
	
	vector<string> tokens;
	//Operation *tempOperation = nullptr;
	Operation *tempOperation = NULL;
	string lineBuffer;
	string dataType = "";
	int width = 0;
	while (!inputStream.eof()){
		getline(inputStream, lineBuffer);
		if (!lineBuffer.empty()){
			splitString(lineBuffer, tokens);
			if (isDeclaration(declarations, tokens)){ // For input, output, variable
				if (isDataWidthAndType(DataWidthAndType, tokens)){
					dataType = setDataType(tokens);
					width = setDataWidth(tokens);
					if (tokens.at(0) == "input")
						generatePorts(dataType, width, "INPUT_PORT", tokens, portList);

					if (tokens.at(0) == "output")
						generatePorts(dataType, width, "OUTPUT_PORT", tokens, portList);

					if (tokens.at(0) == "variable")
						generateVariables(dataType, width, tokens, variableList);
				}
			}

			if (isOperation(operationTypes, tokens)){ // For different operationTypes
				if (tokens.size() == 5){
					tempOperation = new Operation(tokens.at(3), tokens.at(2), tokens.at(4), "", tokens.at(0), lineBuffer);
					operationList.push_back(tempOperation);
				}

				if (tokens.size() == 7){
					tempOperation = new Operation(tokens.at(3), tokens.at(2), tokens.at(4), tokens.at(6), tokens.at(0), lineBuffer);
					operationList.push_back(tempOperation);
				}

				setVariableDependency(tempOperation, variableList);
			}
			tokens.clear();
		}
		lineBuffer.clear();
	}
	inputStream.close();
}

void Parser::setVariableDependency(Operation *operation, vector<Variable *> &variableList){
	for (int i = 0; i < variableList.size(); i++){
		if (operation->getOutput() == variableList.at(i)->getName()){
			variableList.at(i)->setInput(operation);
		}
	}

	for (int i = 0; i < operation->inputListSize(); i++){
		for (int j = 0; j < variableList.size(); j++){
			if (operation->getFromInputList(i) == variableList.at(j)->getName()){
				variableList.at(j)->setIntoOutputList(operation);
			}
		}
	}
}

void Parser::splitString(string &lineBuffer, vector<string> &tokens){
	int i = 0;
	vector<char> token;
	vector<string> tempTokens;
	while (i != lineBuffer.size()){
		if (lineBuffer[i] == ' '
			|| lineBuffer[i] == '\t'
			|| lineBuffer[i] == ','
			|| lineBuffer[i] == '\\'
			|| lineBuffer[i] == '.'){
			if (!token.empty()){
				tempTokens.push_back(string(token.begin(), token.end()));
			}
			token.clear();
		}
		else{
			if (lineBuffer[i] != ' '
				|| lineBuffer[i] != '\t'
				|| lineBuffer[i] != ','
				|| lineBuffer[i] != '\\'
				|| lineBuffer[i] != '.')
				token.push_back(lineBuffer[i]);
		}
		i++;
	}
	tempTokens.push_back(string(token.begin(), token.end()));

	i = 0;
	while (i != tempTokens.size() && tempTokens.at(i) != "//" && tempTokens.at(i) != ""){
		tokens.push_back(tempTokens.at(i));
		i++;
	}
}

void Parser::generatePorts(string dataType, int width, string portType, vector<string> &tokens, vector<Port *> &portList){
	//Port *tempPort = nullptr;
	Port *tempPort = NULL;
	int i;
	if (dataType == "UInt"){
		if (tokens.at(1) == "unsigned")
			i = 3;
		else
			i = 2;
	}
	else
		i = 2;
	
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "bool")
		width = 1;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "char")
		width = 8;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "short")
		width = 16;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "int")
		width = 32;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "long")
		width = 64;
	if (tokens.at(1) == "bool")
		width = 1;
	if (tokens.at(1) == "char")
		width = 8;
	if (tokens.at(1) == "short")
		width = 16;
	if (tokens.at(1) == "int")
		width = 32;
	if (tokens.at(1) == "long")
		width = 64;
	if (tokens.at(1) == "Int1" || tokens.at(1) == "UInt1")
		width = 1;
	if (tokens.at(1) == "Int8" || tokens.at(1) == "UInt8")
		width = 8;
	if (tokens.at(1) == "Int16" || tokens.at(1) == "UInt16")
		width = 16;
	if (tokens.at(1) == "Int32" || tokens.at(1) == "UInt32")
		width = 32;
	if (tokens.at(1) == "Int64" || tokens.at(1) == "UInt64")
		width = 64;


	
	
	cout << "the current width of the is: " << width << endl;

	while (i != tokens.size()){
		tempPort = new Port(tokens.at(i), width, portType, dataType);
		portList.push_back(tempPort);
		i++;
	}
}


void Parser::generateVariables(string dataType, int width, vector<string> &tokens, vector<Variable *> &variableList){
	//Variable *tempVariable = nullptr;
	Variable *tempVariable = NULL;
	int i;
	if (dataType == "UInt"){
		if (tokens.at(1) == "unsigned")
			i = 3;
		else
			i = 2;
	}
	else
		i = 2;

	if (tokens.at(1) == "unsigned" && tokens.at(2) == "bool")
		width = 1;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "char")
		width = 8;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "short")
		width = 16;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "int")
		width = 32;
	if (tokens.at(1) == "unsigned" && tokens.at(2) == "long")
		width = 64;
	if (tokens.at(1) == "bool")
		width = 1;
	if (tokens.at(1) == "char")
		width = 8;
	if (tokens.at(1) == "short")
		width = 16;
	if (tokens.at(1) == "int")
		width = 32;
	if (tokens.at(1) == "long")
		width = 64;
	if (tokens.at(1) == "Int1" || tokens.at(1) == "UInt1")
		width = 1;
	if (tokens.at(1) == "Int8" || tokens.at(1) == "UInt8")
		width = 8;
	if (tokens.at(1) == "Int16" || tokens.at(1) == "UInt16")
		width = 16;
	if (tokens.at(1) == "Int32" || tokens.at(1) == "UInt32")
		width = 32;
	if (tokens.at(1) == "Int64" || tokens.at(1) == "UInt64")
		width = 64;

	cout << "the current width of the is: " << width << endl;

	while (i != tokens.size()){
		tempVariable = new Variable(tokens.at(i), dataType, width);
		variableList.push_back(tempVariable);
		i++;
	}
}
