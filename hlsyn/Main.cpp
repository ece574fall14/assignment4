#include "Main.h"

//to_string
void usage(){
	cout << "The using format is:" << endl;
	cout << "	hlsyn cFile latency verilogFile" << endl;
}

int main(int argc, char *argv[]){
	if (argc != 4){
		usage();
		exit(1);
	}
	clock_t begin, end;
	begin = clock();
	string inputLatency(argv[2]);
	AssistList assistList;
	Parser::parsingCFile(string(argv[1]), assistList.portList, assistList.variableList, assistList.dependencyGraph.getOperationList());
	cout << "Building the dependency graph, building graph connection\n" << endl;
	assistList.dependencyGraph.buildGraphConnection(assistList.portList, assistList.variableList);
	cout << "Calculating the minimum required latency for target graph\n" << endl;
	int minLatency = assistList.dependencyGraph.computeMaximCycles();
	cout << "Checking the validity of the latency value input by users\n" << endl;
	if (stoi(inputLatency) < minLatency){
		cout << "Latency cannot less than " << to_string(static_cast<long long>(minLatency)) << endl;
		exit(1);
	}
	cout << "Set latency for the dependency graph\n" << endl;
	assistList.dependencyGraph.setLatency(stoi(inputLatency));
	cout << "Start Force Directed Scheduling!!\n" << endl;
	assistList.dependencyGraph.forceDirectedScheduling(assistList.timeList);
	cout << "Generating verilog code for the target cFile\n" << endl;
	CircuitTools::generateVerilogCode(stoi(inputLatency), assistList.portList, assistList.variableList, assistList.timeList, string(argv[3]));
	cout << "Perfectly Done\n" << endl;
	end = clock();
	printf("The program takes: %f seconds to complete the whole job", (float)(end - begin)/CLOCKS_PER_SEC);
	return 0;
}